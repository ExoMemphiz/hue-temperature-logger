# Hue Temperature Logger

Philips Hue Motion Sensors also has temperature sensors built in, and their readings can be accessed through the Hue API.
I thought it would be fun to save the temperature readings regularly throughout the day and use it to plot the temperature in my home.
The goal with this project is to do the logging, and to practice using TypeScript etc.

## Setup

1. Get a username for your Hue Bridge as [described here](https://developers.meethue.com/documentation/getting-started).  
2. Make a copy of `config-template.ts` called `config.ts`, put your username in it and place the file into the src folder.
3. `npm install`
4. `npm run build`
5. `npm run main`
